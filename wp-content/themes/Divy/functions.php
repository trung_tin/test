<?php

function custom_enqueue_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_style('custom-css', get_stylesheet_directory_uri() . '/custom/custom.css');
    wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/custom/custom.js', array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', 'custom_enqueue_scripts');

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action('woocommerce_after_single_product', 'custom_related_products', 5);

function custom_related_products() {
    global $product, $wpdb;

    $count = $wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->posts WHERE post_type = 'product' AND post_status = 'publish'");
    $related = wc_get_related_products($product->get_id(),$count);

    if ($related) {
        echo '<section class="related products">
            <h2>Related products</h2>
        ';
        woocommerce_product_loop_start();
        foreach ($related as $related_product) {
            $post_object = get_post($related_product);
            setup_postdata($GLOBALS['post'] =& $post_object);
            wc_get_template_part('content', 'product');
        }
        woocommerce_product_loop_end();
        echo '</section>';
    }
}

add_shortcode( 'cart_count', 'cart_count_shortcode' );
function cart_count_shortcode() {
    ob_start();
    $count = WC()->cart->get_cart_contents_count();
    $image_url = get_stylesheet_directory_uri() . '/images/shopping-cart.png';
    ?>
    <span class="cart-count-display">
        <a href="/cart">
            <img src="<?php echo $image_url; ?>" alt="cart" >
            <b><?php echo $count; ?></b>
        </a>
    </span>
    <?php
    $output = ob_get_clean();
    return $output;
}
